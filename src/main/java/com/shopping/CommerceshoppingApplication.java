package com.shopping;

import com.shopping.model.*;
import com.shopping.repository.RoleRepository;
import com.shopping.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class CommerceshoppingApplication implements CommandLineRunner {

    private final UserService userService;
    private final ProductService productService;
    private final OrderService orderService;
    private final FeedbackService feedbackService;
    private final AdminUserService adminUserService;
    private final AdminProductService adminProductService;
    private final AdminFeedbackService adminFeedbackService;
    private final RoleRepository roleRepository;


    @Autowired
    public CommerceshoppingApplication(UserService userService, ProductService productService, OrderService orderService, FeedbackService feedbackService, AdminUserService adminUserService, AdminProductService adminProductService, AdminFeedbackService adminFeedbackService, RoleRepository roleRepository) {
        this.userService = userService;
        this.productService = productService;
        this.orderService = orderService;
        this.feedbackService = feedbackService;
        this.adminUserService = adminUserService;
        this.adminProductService = adminProductService;
        this.adminFeedbackService = adminFeedbackService;
        this.roleRepository = roleRepository;
    }


    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.shopping")).build();
    }

    public static void main(String[] args) {
        SpringApplication.run(CommerceshoppingApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        Role role = new Role();
        role.setName("admin");
        roleRepository.save(role);

        Role role2 = new Role();
        role2.setName("user");
        roleRepository.save(role2);


        User user = new User();
        user.setName("Roop");
        user.setAge(21);
        user.setPhoneNumber("+37256893453");
        user.setPassword("$2a$10$5hNN3p3WTBz1bhimOt8ee.2oKEp9sTKso.6cd5YqfoB5ojvxrG/0u");
        user.setUsername("user");
        user.setRole(role2);

        User users1 = new User();
        users1.setName("Serhan");
        users1.setAge(21);
        users1.setPhoneNumber("+37256893453");
        users1.setPassword("$2a$10$5hNN3p3WTBz1bhimOt8ee.2oKEp9sTKso.6cd5YqfoB5ojvxrG/0u");
        users1.setUsername("admin");
        users1.setRole(role);



        Order order = new Order();
        order.setName("Laptop");
        order.setQuantity(1);
        order.setPrice(750.50f);
        order.setStatus(true);
        user.getOrderSet().add(order);

        Product product = new Product();
        product.setName("Banana");
        product.setPrice(10.50f);


        Feedback feedback = new Feedback();
        feedback.setName("James");
        feedback.setMessage("I am very happy with the user journey");
        user.getUserFeedback().add(feedback);

        System.out.println(userService.save(user));
        System.out.println(userService.save(users1));

        System.out.println(orderService.save(order));

        System.out.println(productService.save(product));

        System.out.println(feedbackService.save(feedback));

    }
}
