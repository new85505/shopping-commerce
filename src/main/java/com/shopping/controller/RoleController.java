package com.shopping.controller;

import com.shopping.model.Role;
import com.shopping.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/role")
@RestController
public class RoleController {

    final RoleService roleService;


    @Autowired
    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PostMapping(name = "/add")
    public Role addRole(@RequestBody Role role) {
        return roleService.add(role);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @GetMapping(value = "/{id}")
    public Role getRole(@PathVariable Long id) {
        return roleService.getRole(id);
    }
}
