package com.shopping.controller;


import com.shopping.exceptions.ProductNotFoundException;
import com.shopping.exceptions.UserNotFoundException;
import com.shopping.model.Admin;
import com.shopping.model.Feedback;
import com.shopping.model.Product;
import com.shopping.model.User;
import com.shopping.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.jws.soap.SOAPBinding;

@RestController
public class AdminController {

    private final AdminFeedbackService adminFeedbackService;
    private final AdminProductService adminProductService;
    private final ProductService productService;
    private FeedbackService feedbackService;
    private final AdminUserService adminUserService;
    private final UserService userService;

    @Autowired
    public AdminController(AdminFeedbackService adminFeedbackService,
                           AdminProductService adminProductService,
                           ProductService productService,
                           FeedbackService feedbackService,
                           AdminUserService adminUserService,
                           UserService userService) {
        this.adminFeedbackService = adminFeedbackService;
        this.adminProductService = adminProductService;
        this.productService = productService;
        this.feedbackService = feedbackService;
        this.adminUserService = adminUserService;
        this.userService = userService;
    }


    @GetMapping(value = "/admin/user-feedback/{id}")
    public ResponseEntity<Feedback> findFeedbackByName(@PathVariable("id") Long id) {
        User user = userService.findById(id);
        Feedback feedbackById = adminFeedbackService.findFeedbackById(user);

        if (feedbackById != null)
            return new ResponseEntity(feedbackById, HttpStatus.OK);
        else
            return new ResponseEntity("Feedback not found for " + id, HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/admin")
    public ResponseEntity<Product> saveProducts(@RequestBody Product product) {
        if (productService.findById(product.getId()) == null) {
            Product addProduct = adminProductService.saveProduct(product);
            return new ResponseEntity(addProduct, HttpStatus.CREATED);
        } else
            return new ResponseEntity("Product already exists", HttpStatus.CONFLICT);
    }

    @DeleteMapping(value = "/admin/product/delete/{id}")
    public ResponseEntity<Product> deleteProduct(@RequestBody Product product) throws ProductNotFoundException {
        if (productService.findById(product.getId()) == null) {
            throw new ProductNotFoundException("product not found with the " + product.getId());
        }
        return new ResponseEntity(adminProductService.delete(product.getId()), HttpStatus.OK);
    }

    @GetMapping(value = "/admin/user/name/{name}")
    public ResponseEntity<User> findUserByName(@PathVariable("name") String name, User user) {
        User findUser = adminUserService.findUserByName(user.getName());

        if (findUser != null) {
            return new ResponseEntity(findUser, HttpStatus.OK);
        } else
            return new ResponseEntity("No name found ", HttpStatus.NOT_FOUND);
    }
}
