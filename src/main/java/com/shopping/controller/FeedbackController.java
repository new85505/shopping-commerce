package com.shopping.controller;


import com.shopping.model.Feedback;
import com.shopping.model.User;
import com.shopping.service.FeedbackService;
import com.shopping.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;


@RestController
public class FeedbackController {

    private final FeedbackService feedbackService;
    private final UserService userService;

    @Autowired
    public FeedbackController(FeedbackService feedbackService, UserService userService) {
        this.feedbackService = feedbackService;
        this.userService = userService;
    }

    @GetMapping(value = "/feedback/{id}")
    public ResponseEntity<Feedback> findById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity(feedbackService.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity("No id found" + id, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/feedback/name/{name}")
    public ResponseEntity<Feedback> findByName(@PathVariable("name") String username) {
        User user = userService.findByName(username);
        Set<Feedback> feedbackSet  = user.getUserFeedback();
        if (feedbackSet != null)
            return new ResponseEntity(feedbackSet, HttpStatus.OK);
        else
            return new ResponseEntity("Feedback not found for " + username, HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/feedback")
    public ResponseEntity<Feedback> save(@RequestBody Feedback feedback) {
//        if (feedbackService.findByName(feedback.getName()) == null) {
            Feedback saveFeedback = feedbackService.save(feedback);
            return new ResponseEntity(saveFeedback, HttpStatus.CREATED);
//        } else
//            return new ResponseEntity("feedback exists", HttpStatus.CONFLICT);
    }
}
