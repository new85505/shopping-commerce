package com.shopping.controller;

import com.shopping.exceptions.EmailAlreadyExistsException;
import com.shopping.exceptions.UserNameAlreadyExistsException;
import com.shopping.exceptions.UserNotFoundException;
import com.shopping.model.User;
import com.shopping.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@RequestMapping("/user")
@RestController
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/list")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<User> roleList() {
        return userService.getUserList();
    }



    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public User registerUser(@RequestBody User user) throws EmailAlreadyExistsException, UserNameAlreadyExistsException {
        return userService.register(user);
    }



    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response){
        return userService.logout(request,response);
    }


    @GetMapping(value = "/user/{id}")
    public ResponseEntity<User> findUserById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity(userService.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity("Id not found", HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping(value = "/users")
    public ResponseEntity<User> save(@RequestBody User user) {
        if (userService.findByName(user.getName()) == null) {
            User saveUsers = userService.save(user);
            return new ResponseEntity(saveUsers,HttpStatus.CREATED);
//            if (saveUsers.getId() == null)
//                return new ResponseEntity("Users already exists", HttpStatus.CONFLICT);
//            else
//                return new ResponseEntity("Users not present", HttpStatus.CREATED);
        } else
            return new ResponseEntity("Name already exists", HttpStatus.CONFLICT);
    }

    @DeleteMapping(value = "/user/delete/{id}")
    public ResponseEntity<User> delete(@PathVariable("id") Long id) throws UserNotFoundException {
        if (userService.findById(id) == null)
            throw new UserNotFoundException("user not found with the " + id);
        return new ResponseEntity(userService.delete(id), HttpStatus.OK);
    }

    @PatchMapping(value = "/users/{id}/phoneNumber")
    public ResponseEntity<User> update(@PathVariable("id") Long id, @RequestParam("phone") String phoneNumber) {
        User user = userService.update(phoneNumber, id);
        return new ResponseEntity(user, HttpStatus.NOT_ACCEPTABLE);
    }
}
