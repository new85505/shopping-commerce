package com.shopping.controller;

import com.shopping.exceptions.ProductNotFoundException;
import com.shopping.model.Product;
import com.shopping.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "/product/{id}")
    public ResponseEntity<Product> findById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity(productService.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity("Id not found", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/product/name/{name}")
    public ResponseEntity<Product> findByName(@PathVariable("name") String name) {

        Product product = productService.findByName(name);

        if (product != null) {
            return new ResponseEntity(product, HttpStatus.OK);
        } else
            return new ResponseEntity("No name found", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping(value = "/product/delete/{id}")
    public ResponseEntity<Product> deleteCart(@PathVariable(name = "id") Long id) throws ProductNotFoundException {

        if (productService.findById(id) == null)
            throw new ProductNotFoundException("Product not found with the " + id);

        return new ResponseEntity(productService.deleteCart(id), HttpStatus.OK);
    }

    @PostMapping(value = "/product")
    public ResponseEntity<Product> save(@RequestBody Product product) {
            Product saveProduct = productService.save(product);

            return new ResponseEntity(saveProduct, HttpStatus.CREATED);

        } //else
            //return new ResponseEntity("product name already exists", HttpStatus.CONFLICT);
    }

