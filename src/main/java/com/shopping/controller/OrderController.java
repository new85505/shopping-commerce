package com.shopping.controller;

import com.shopping.model.Order;
import com.shopping.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class OrderController {

    private final OrderService orderService;

    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @GetMapping(value = "/order/{id}")
    public ResponseEntity<Order> findById(@PathVariable("id") Long id) {
        try {
            return new ResponseEntity(orderService.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity("No id found", HttpStatus.NOT_FOUND);
        }

    }

    @GetMapping(value = "/order/name/{name}")
    public ResponseEntity<Order> findByName(@PathVariable("name") String name) {
        Order order = orderService.findByName(name);

        if (order != null)
            return new ResponseEntity(order, HttpStatus.OK);
        else
            return new ResponseEntity("No name found", HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/order")
    public ResponseEntity<Order> save(@RequestBody Order order) {
        if (orderService.findByName(order.getName()) == null) {
            Order saveOrder = orderService.save(order);
            return new ResponseEntity(saveOrder, HttpStatus.CREATED);

            //if (saveOrder.getId() != null)
              //  return new ResponseEntity("order exists", HttpStatus.CONFLICT);
            //else
              //  return new ResponseEntity("order not present", HttpStatus.CREATED);
        } else
            return new ResponseEntity("name already exists", HttpStatus.CONFLICT);

    }
}
