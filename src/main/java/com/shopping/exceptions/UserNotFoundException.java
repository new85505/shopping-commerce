package com.shopping.exceptions;

public class UserNotFoundException extends Throwable {
    public UserNotFoundException(String msg) {
        System.out.println(msg);
    }
}
