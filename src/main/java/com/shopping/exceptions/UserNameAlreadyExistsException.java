package com.shopping.exceptions;

public class UserNameAlreadyExistsException extends Throwable {
    public UserNameAlreadyExistsException(String username) {
        super();
    }
}
