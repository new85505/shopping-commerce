package com.shopping.exceptions;

public class ProductNotFoundException extends Throwable {
    public ProductNotFoundException(String msg){
        System.out.println(msg);
    }
}
