package com.shopping.repository;

import com.shopping.model.Feedback;
import com.shopping.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeedbackRepository extends JpaRepository<Feedback,Long> {
    Feedback findByName(String name);
    Feedback findByUser(Long userId);
}
