package com.shopping.service;

import com.shopping.model.Order;
import com.shopping.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Override
    public Order findById(Long id) {
        return orderRepository.findById(id).get();
    }

    @Override
    public Order findByName(String name) {
        return orderRepository.findByName(name);
    }

    @Override
    public Order save(Order order) {
        return orderRepository.save(order);
    }
}
