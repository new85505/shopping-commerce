package com.shopping.service;


import com.shopping.model.Role;
import com.shopping.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    final RoleRepository roleRepository;

    @Autowired
    private RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;

    }


    @Override
    public Role add(Role role) {
        Role savedRole = roleRepository.save(role);
        return savedRole;
    }

    @Override
    public Role getByName(String name) {
        return roleRepository.findByUsername(name);
    }

    @Override
    public Role getRole(Long id) {
        return roleRepository.findById(id).filter(u -> u.getId().equals(id)).orElse(null);
    }

}
