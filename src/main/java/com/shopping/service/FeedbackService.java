package com.shopping.service;

import com.shopping.model.Feedback;

public interface FeedbackService {
      public Feedback findById(Long id);
      public Feedback findByName(String name);
      public Feedback save(Feedback feedback);
}
