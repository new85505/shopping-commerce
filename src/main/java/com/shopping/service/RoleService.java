package com.shopping.service;

import com.shopping.model.Role;

public interface RoleService {
    Role getRole(Long id);

    Role add(Role role);

    Role getByName(String name );
}
