package com.shopping.service;

import com.shopping.model.Order;

public interface OrderService {
    public Order findById(Long id);
    public Order findByName(String name);
    public Order save(Order order);
}
