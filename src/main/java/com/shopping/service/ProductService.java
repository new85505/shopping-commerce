package com.shopping.service;

import com.shopping.model.Product;

public interface ProductService {
    public Product findById(Long id);
    public Product findByName(String name);
    public String deleteCart(Long id);
    public String addCart(Product product);
    public Product save(Product product);
}
