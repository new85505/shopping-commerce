package com.shopping.service;


import com.shopping.model.Feedback;
import com.shopping.model.User;
import com.shopping.repository.FeedbackRepository;
import com.shopping.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminFeedbackServiceImpl implements AdminFeedbackService {

    private final FeedbackRepository feedbackRepository;

    @Autowired
    public AdminFeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public Feedback findFeedbackById(User user) {
        return feedbackRepository.findByUser(user.getId());
    }
}
