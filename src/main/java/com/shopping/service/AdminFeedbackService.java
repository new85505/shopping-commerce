package com.shopping.service;

import com.shopping.model.Feedback;
import com.shopping.model.User;


public interface AdminFeedbackService {
    public Feedback findFeedbackById(User user);
}
