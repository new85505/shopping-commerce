package com.shopping.service;

import com.shopping.model.Admin;
import com.shopping.model.Product;

public interface AdminProductService {
    public String delete(Long id);
    public Product saveProduct(Product product);
}
