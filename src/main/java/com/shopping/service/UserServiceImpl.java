package com.shopping.service;

import com.shopping.exceptions.EmailAlreadyExistsException;
import com.shopping.exceptions.UserNameAlreadyExistsException;
import com.shopping.model.Role;
import com.shopping.model.User;
import com.shopping.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final RoleService roleService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, RoleService roleService) {
        this.userRepository = userRepository;
        this.roleService = roleService;
    }

    @Override
    public User findById(Long id) {

        return userRepository.findById(id).get();
    }

    @Override
    public User findByName(String name) {
        return userRepository.findByName(name);
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public String delete(Long id) {
        userRepository.deleteById(id);
        return "User deleted";
    }

    @Override
    public User update(String phoneNumber, Long id) {
        if (userRepository.findById(id).isPresent()) {
            User user = userRepository.findById(id).get();
            user.setPhoneNumber(phoneNumber);
            return userRepository.save(user);
        }
        return null;
    }

    @Override
    public List<User> getUserList() {
        return userRepository.findAll();
    }

    @Override
    public User register(User user) throws EmailAlreadyExistsException, UserNameAlreadyExistsException {
        if (userRepository.findByUsername(user.getUsername()) == null) {
            if (userRepository.findByEmail(user.getEmail()) == null) {
                Role role = roleService.getByName(user.getRole().getName());
                user.setRole(role);
                return userRepository.save(user);
            } else throw new EmailAlreadyExistsException(user.getEmail());
        } else throw new UserNameAlreadyExistsException(user.getUsername());
    }

    @Override
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "Success";
    }
}

