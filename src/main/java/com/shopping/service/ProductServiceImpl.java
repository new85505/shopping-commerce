package com.shopping.service;

import com.shopping.model.Product;
import com.shopping.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Product findById(Long id) {
        return productRepository.findById(id).get();
    }

    @Override
    public Product findByName(String name) {
        return productRepository.findByName(name);
    }

    @Override
    public String deleteCart(Long id) {
        productRepository.deleteById(id);
        return "product deleted";
    }

    @Override
    public String addCart(Product product) {
       productRepository.save(product);
       return "Added successfully";
    }

    @Override
    public Product save(Product product) {
        return productRepository.save(product);
    }
}
