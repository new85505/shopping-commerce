package com.shopping.service;

import com.shopping.model.User;

public interface AdminUserService {
    public User findUserByName(String name);
}
