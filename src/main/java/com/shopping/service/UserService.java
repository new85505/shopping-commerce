package com.shopping.service;

import com.shopping.exceptions.EmailAlreadyExistsException;
import com.shopping.exceptions.UserNameAlreadyExistsException;
import com.shopping.model.Feedback;
import com.shopping.model.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public interface UserService {
  public User findById(Long id);
  public User findByName(String name);
  public User save(User users);
  public String delete(Long id);
  public User update(String phoneNumber, Long id);
  List<User> getUserList();

  User register(User user) throws EmailAlreadyExistsException, UserNameAlreadyExistsException;

  String logout(HttpServletRequest request, HttpServletResponse response);

}
