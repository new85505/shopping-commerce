package com.shopping.service;

import com.shopping.model.Product;
import com.shopping.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminProductServiceImpl implements AdminProductService {

    private final ProductRepository productRepository;

    @Autowired
    public AdminProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String delete(Long id) {
        productRepository.deleteById(id);
        return "Product deleted";
    }

    @Override
    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }
}
