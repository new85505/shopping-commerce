package com.shopping.service;

import com.shopping.model.Feedback;
import com.shopping.repository.FeedbackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FeedbackServiceImpl implements FeedbackService {

    private final FeedbackRepository feedbackRepository;

    @Autowired
    public FeedbackServiceImpl(FeedbackRepository feedbackRepository) {
        this.feedbackRepository = feedbackRepository;
    }

    @Override
    public Feedback findById(Long id) {
        return feedbackRepository.findById(id).get();
    }

    @Override
    public Feedback findByName(String name) {
        return feedbackRepository.findByName(name);
    }

    @Override
    public Feedback save(Feedback feedback) {
        return feedbackRepository.save(feedback);
    }
}
