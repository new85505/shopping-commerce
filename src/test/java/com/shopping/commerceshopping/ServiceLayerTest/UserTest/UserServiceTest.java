package com.shopping.commerceshopping.ServiceLayerTest.UserTest;

import com.shopping.model.User;
import com.shopping.repository.UserRepository;
import com.shopping.service.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Autowired
    UserServiceImpl userService;

    @Autowired
    UserRepository userRepository;

    @Before
    public void setUp(){
        User user = new User();
        user.setName("Roop");
        user.setPhoneNumber("+372478593983");
        user.setEmailAddress("r@gmail.com");
        user.setAge(21);
        System.out.println(user.getId());
    }

    @Test
    public void findByIdTest(){
        //UserServiceImpl userService = new UserServiceImpl(userRepository, roleService);
        User user = userService.findById(1l);
        Assert.assertNotNull(user.getId());
    }

    @Test
    public void findByNameTest(){
        //UserServiceImpl userService = new UserServiceImpl(userRepository, roleService);
        User user = userService.findByName("Serhan");
        Assert.assertNotNull(user.getName());
    }

    @Test
    public void saveTest(){
        //UserServiceImpl userService = new UserServiceImpl(userRepository, roleService);
        User user = new User();
        user.setName("Roop");
        user.setAge(21);
        user.setPhoneNumber("+37245678934");
        user.setEmailAddress("r@gmail.com");
        User user1 = userService.save(user);
        Assert.assertNotNull(user1.getId());
    }

    @Test
    public void deleteTest(){
        //UserServiceImpl userService = new UserServiceImpl(userRepository, roleService);
        String user = userService.delete(1l);
        Assert.assertNotNull(user);
    }

    @Test
    public void updateTest(){
        //UserServiceImpl userService = new UserServiceImpl(userRepository,roleService);
        User user = userService.update("+326895432",1l);
        Assert.assertNotNull(user);
    }
}
