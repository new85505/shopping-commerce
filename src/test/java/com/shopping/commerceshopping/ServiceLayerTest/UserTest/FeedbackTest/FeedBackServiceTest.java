package com.shopping.commerceshopping.ServiceLayerTest.UserTest.FeedbackTest;

import com.shopping.model.Feedback;
import com.shopping.repository.FeedbackRepository;
import com.shopping.service.FeedbackServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class FeedBackServiceTest {

    @Mock
    FeedbackRepository feedbackRepository;

    private FeedbackServiceImpl feedbackService = new FeedbackServiceImpl(feedbackRepository);

    @Before
    public void beforeEachTest(){
        Feedback feedback = new Feedback();
        feedback.setId(3l);
        feedback.setName("Carlos");
        feedback.setMessage("Hello");
        feedbackRepository.save(feedback);
    }

    @Test
    public void findByIdTest() {
        Feedback feedback = feedbackService.findById(3l);
        Assert.assertNotNull(feedback.getId());
    }

    @Test
    public void findByNameTest() {
        Feedback feedback = feedbackService.findByName("Roop");
        Assert.assertNotNull(feedback.getName());
    }

    @Test
    public void saveTest() {
        Feedback feedback = new Feedback();
        feedback.setName("James");
        feedback.setMessage("I am very happy with the user journey");
        Feedback feedback1 = feedbackService.save(feedback);
        Assert.assertNotNull(feedback1.getId());
    }
}
