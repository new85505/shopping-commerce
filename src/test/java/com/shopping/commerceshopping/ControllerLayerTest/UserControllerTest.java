package com.shopping.commerceshopping.ControllerLayerTest;

import com.shopping.controller.UserController;
import com.shopping.exceptions.UserNotFoundException;
import com.shopping.model.User;
import com.shopping.repository.UserRepository;
import com.shopping.service.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserControllerTest {

    @InjectMocks
    UserController userController;

    @Mock
    UserService userService;

    @Test
    public void findById() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        User user = new User();
        user.setName("Roop");
        user.setEmailAddress("r@gmail.com");
        user.setPhoneNumber("+3724578993");
        when(userService.findById(anyLong())).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.findUserById(1l);

        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void findByName() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        User user = new User();
        user.setName("Whoever");
        user.setEmailAddress("r@gmail.com");
        user.setPhoneNumber("+3724578993");
        when(userService.findByName(anyString())).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.findUserById(1l);

        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void save() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        User user = new User();
        user.setName("Whoever");
        user.setEmailAddress("r@gmail.com");
        user.setPhoneNumber("+3724578993");
        when(userService.save(any(User.class))).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.save(user);

        Assert.assertEquals(201, responseEntity.getStatusCodeValue());
    }

    @Test
    public void delete() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        User user = new User();
        user.setName("Serhan");
        user.setPhoneNumber("37256893453");
        when(userService.delete(anyLong())).thenReturn(user.getPhoneNumber());

        try {
            ResponseEntity<User> responseEntity = userController.delete(6l);
            Assert.assertEquals(201, responseEntity.getStatusCodeValue());
        }catch (UserNotFoundException e){

        }
    }

    @Test
    public void update() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        User user = new User();
        user.setId(1l);
        user.setPhoneNumber("37245689432567");
        user.setAge(12);
        when(userService.update(anyString(), anyLong())).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.update(1l, "37245689432567");

        Assert.assertEquals("37245689432567",responseEntity.getBody().getPhoneNumber());
    }

}
